class_name TestPurse
extends Node

func _ready():
	pass

func test_purse():
	setting_positive_coins_succeeds()
	setting_negative_coins_fails()
	adding_coins_succeeds()
	removing_coins_succeeds()
	removing_coins_empty_fails()

func setting_positive_coins_succeeds():
	var purse = Purse.new()
	purse.coins = 5
	assert(purse.coins == 5, "The number of coins should be 5")

func setting_negative_coins_fails():
	var purse = Purse.new()
	purse.coins = -5
	assert(purse.coins == 0, "The number of coins should be 0")

func adding_coins_succeeds():
	var purse = Purse.new()
	purse.add_coins(3)
	assert(purse.coins == 3, "The number of coins should be 3")

func removing_coins_succeeds():
	var purse = Purse.new()
	purse.set_coins(5)
	purse.remove_coins(1)
	assert(purse.coins == 4, "The number of coins should be 4")

func removing_coins_empty_fails():
	var purse = Purse.new()
	purse.remove_coins(1)
	assert(purse.coins == 0, "The number of coins should be 0")
