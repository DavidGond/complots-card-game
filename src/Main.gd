extends Node

onready var _server := Server.new()
onready var _client := Client.new()

onready var _message: Label = $Message

onready var _hosting_window: Control = $HostingWindow
onready var _joining_window: Control = $JoiningWindow

onready var _hosting_port: LineEdit = $HostingWindow/HostingSettings/Port/ValuePort
onready var _max_players: SpinBox = $HostingWindow/HostingSettings/MaxPlayers/ValueMaxPlayers

onready var _joining_ip: LineEdit = $JoiningWindow/JoiningSettings/IP/ValueIP 
onready var _joining_port: LineEdit = $JoiningWindow/JoiningSettings/Port/ValuePort

onready var _name_box: Container = $Pseudo
onready var _name: LineEdit = $Pseudo/ValuePseudo

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	get_tree().set_auto_accept_quit(true)
	add_child(_server)
	add_child(_client)
	resetMessage()
	_name_box.visible = false
	_hosting_window.visible = false
	_joining_window.visible = false

func _on_Host_pressed() -> void:
	resetMessage()
	_name_box.visible = true
	_hosting_window.visible = true

func _on_Join_pressed() -> void:
	resetMessage()
	_name_box.visible = true
	_joining_window.visible = true

func _on_BeginHosting_pressed() -> void:
	resetMessage()
	var ready_to_host = true
	if !check_port(_hosting_port.text):
		ready_to_host = false
		displayMessage("Entered port is not valid!")
	if _name.text.trim_prefix(" ").empty():
		ready_to_host = false
		displayMessage("Entered name must not be blank!")
	if ready_to_host:
		Lobby.my_info.name = _name.text
		var max_players = _max_players.value
		_server.init(int(_hosting_port.text), max_players)
		Lobby.maximum_players = max_players
		join_server_lobby()

func _on_CancelHosting_pressed() -> void:
	resetMessage()
	if get_tree().is_network_server():
		_server.stop()
	_name_box.visible = false
	_hosting_window.visible = false

func _on_CancelJoining_pressed() -> void:
	resetMessage()
	_name_box.visible = false
	_joining_window.visible = false

func _on_BeginJoining_pressed() -> void:
	resetMessage()
	var ip = _joining_ip.text
	var ready_to_join = true
	if !ip.is_valid_ip_address():
		ready_to_join = false
		displayMessage("Entered IP is not valid!")
	if !check_port(_joining_port.text):
		ready_to_join = false
		displayMessage("Entered port is not valid!")
	if _name.text.trim_prefix(" ").empty():
		ready_to_join = false
		displayMessage("Entered name must not be blank!")
	if ready_to_join:
		Lobby.my_info.name = _name.text
		_client.init(_joining_ip.text, int(_joining_port.text))
		join_client_lobby()

func displayMessage(var message: String) -> void:
	_message.text = message
	_message.visible = true

func check_port(var port: String) -> bool:
	if !port.is_valid_integer():
		return false
	var port_value = int(port)
	if port_value < 0 or port_value > 65353:
		return false
	return true

func resetMessage() -> void:
	_message.text = ""
	_message.visible = false

func join_client_lobby() -> void:
	get_tree().change_scene_to(Const.get_client_lobby())

func join_server_lobby() -> void:
	get_tree().change_scene_to(Const.get_server_lobby())
