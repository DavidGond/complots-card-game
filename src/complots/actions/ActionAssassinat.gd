class_name ActionAssassinat
extends Action

var _game


func _execute(game, _player: Player):
	print("Assassinat !")
	_game = game
	game.connect("card_clicked", self, "card_clicked")
	game.select_card()


func card_clicked(card: Card, target: Player):
	if target.id == _game.current_player().id:
		print("Assassinate cancelled - Cannot discard own cards")
		return
	_game.disconnect("card_clicked", self, "card_clicked")
	_game.player_discard_card(target.id, card)
	_game.stop_select_card()
	_completed()
