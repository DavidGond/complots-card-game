# Base class for all the actions a player can do at the beginning of
# their turn.

# Each Action must implement a 'execute()' method and call
# the '_completed' method when the action is fully completed.

class_name Action
extends Node

signal action_completed


func execute(game: Game, player: Player):
	_execute(game, player)


# Overridden by child classes
func _execute(_game: Game, _player: Player):
	pass


func _completed():
	emit_signal("action_completed")
