class_name Game
extends Control

signal card_pick_confirmed(player_id, character)
signal card_clicked(card, player)
signal player_selected(player)

var current_player_index: int = -1
var order_players := []
var is_current_turn_completed: bool = false
var all_players := {}
var id_players_init := []

onready var board = $Board
onready var local_player := $NetworkPlayer


func _ready():
	local_player.game = self
	var players = {}
	for id in Lobby.player_info:
		var player = Const.get_player_instance()
		player.init(id, Lobby.player_info[id]["name"])
		players[id] = player

	(board.pick_card_window as PickCardWindow).connect("card_picked", self, "_card_picked")
	_init_game(players)


func _init_game(players):
	get_tree().paused = true
	all_players = players
	order_players = players.values()
	assert(players.size() >= 2 and players.size() <= 8, "Number of players must be between 2 and 8")
	var other_players = []
	for player in players.values():
		player.name = str(player.id)  # Helps the RPC calls to identify each player
		if player.id == get_tree().get_network_unique_id():
			local_player.player = player
			var err := local_player.connect("played", self, "_on_player_played")
			if err:
				print("[ERROR] Could not connect to 'played' signal")
				print_stack()
		else:
			other_players.append(player)
	board.init_board(self, local_player.player, other_players)

	rpc_id(1, "init_completed")


remotesync func init_completed():
	var who = get_tree().get_rpc_sender_id()

	for player in all_players.values():
		if player.id == who and not who in id_players_init:
			id_players_init.append(player.id)
			break
	if id_players_init.size() == all_players.size():
		rpc("all_ready")


remotesync func all_ready():
	# Only the server can unpause everyone
	if get_tree().get_rpc_sender_id() == 1:
		get_tree().paused = false

	# Only the server can manage the turns
	if get_tree().get_network_unique_id() == 1:
		# Distribute the coins needed to begin the game
		for player in all_players.values():
			player.rpc("give_coins", 2)
		# Distribute their cards
		_init_deck(all_players.size())


func next_turn():
	if get_tree().get_network_unique_id() != 1:
		return

	var current_player: Player = current_player()
	if is_current_turn_completed or current_player_index < 0:
		is_current_turn_completed = false
		current_player_index = (current_player_index + 1) % all_players.size()
		current_player = current_player()
		rpc("next_player", current_player().id)
	else:
		print(
			(
				"Attempted to skip someone's turn ("
				+ current_player.nickname
				+ ", "
				+ str(current_player.id)
				+ ")"
			)
		)


# Search a player by their id in given array.
# Returns the index in the array if found, -1 otherwise.
func _get_player_index_by_id(array: Array, id: int):
	for i in range(array.size()):
		if array[i].id == id:
			return i
	return -1


puppetsync func next_player(player_id: int):
	current_player_index = _get_player_index_by_id(order_players, player_id)
	if player_id == local_player.player.id:
		local_player.play()


func current_player() -> Player:
	return order_players[current_player_index]


func _on_player_played():
	rpc_id(1, "turn_completed")


remotesync func turn_completed():
	if get_tree().get_rpc_sender_id() == current_player().id:
		is_current_turn_completed = true
		next_turn()


func _init_deck(nb_players: int):
	match nb_players:
		2:
			_init_deck_two_players(all_players)
		3, 4, 5, 6:
			_init_deck_three_to_six_players(all_players)
		7, 8:
			_init_deck_seven_to_eight_players(all_players)


func _init_deck_two_players(players):
	print("Initializing for two players")
	var game_init = GameInitTwoPlayers.new()
	game_init.init(self, players)


func _init_deck_three_to_six_players(players):
	print("Initializing for three to six players")
	var game_init = GameInitThreeToSixPlayers.new()
	game_init.init(self, players)


func _init_deck_seven_to_eight_players(players):
	print("Initializing for seven to eight players")
	var game_init = GameInitSevenToEightPlayers.new()
	game_init.init(self, players)


func create_simple_deck() -> Array:
	var deck = [
		Const.DEFINITIONS.CHARACTERS.ASSASSIN,
		Const.DEFINITIONS.CHARACTERS.CAPITAINE,
		Const.DEFINITIONS.CHARACTERS.COMPTESSE,
		Const.DEFINITIONS.CHARACTERS.DUCHESSE,
	]
	var ambassadeur_inquisiteur = Lobby.game_options[Const.Options.AMBASSADEUR_INQUISITEUR]
	deck.push_back(ambassadeur_inquisiteur)

	return deck


# Adds the cards (as characters) to the pick card window
puppetsync func pick_card(from_cards: Array):
	board.pick_card_from_array(from_cards)


func _card_picked(card: Card):
	rpc_id(1, "inform_card_picked", card.card_character)
	(board as Board).display_pick_card_window(false)


func _card_clicked(card: Card, target: Player):
	emit_signal("card_clicked", card, target)


func _player_selected(player: Player):
	emit_signal("player_selected", player)


mastersync func inform_card_picked(character):
	var id := get_tree().get_rpc_sender_id()
	var sender = all_players[id]
	print(
		(
			"Player "
			+ str(sender.nickname)
			+ " chose "
			+ str(Const.DEFINITIONS.CARDS[character]["Name"])
		)
	)

	# Confirm
	give_card_to_player(character, sender)

	# Announce the picked card
	emit_signal("card_pick_confirmed", id, character)


func player_discard_card(target_id, card: Card):
	var target: Player = all_players[target_id]
	var index = target.hand.cards.find(card)
	if index <= -1:
		print("Card not found at index: " + str(index))
		return

	rpc_id(1, "inform_player_discard_card", target_id, index)


mastersync func inform_player_discard_card(target_id, index: int):
	var current_player := current_player()
	var id := get_tree().get_rpc_sender_id()

	if id != current_player.id:
		print("Discard cancelled - Not their turn")
		return

	var sender: Player = all_players[id]
	var target: Player = all_players[target_id]

	var card = target.hand.cards[index]
	if not card:
		print("Invalid discard card index: " + str(index))

	print(
		(
			"Player "
			+ sender.name
			+ " wants to have "
			+ target.name
			+ " discard "
			+ Const.DEFINITIONS.CARDS[card.card_character]["Name"]
		)
	)

	target.rpc("discard_card", index, card.card_character)


mastersync func inform_coins_taken(amount):
	var current_player := current_player()
	var id := get_tree().get_rpc_sender_id()
	var sender = all_players[id]
	print("Player " + str(sender.nickname) + " wants to take " + str(amount) + " coins.")
	if id == current_player().id:
		print("Coins confirmed.")
		current_player.rpc("give_coins", amount)
		return
	print("Coins cancelled - Not their turn")


# Gives a card to a player. Should only used by server.
func give_card_to_player(character, player: Player):
	if !get_tree().is_network_server():
		return

	# Send to player
	player.rpc_id(player.id, "give_card", character)

	# Update local version of the player
	if player.id != 1:
		player.give_card(character)

	# Inform others
	player.rpc("give_hidden_card")


# Highlights the cards that can be selected by the local player
func select_card():
	_set_selection_card_state(true)


# Stop highlighting the cards that can be selected by the local player
func stop_select_card():
	_set_selection_card_state(false)


func _set_selection_card_state(state: bool):
	# Notify the other player's cards they can be clicked on
	for player in all_players.values():
		player = player as Player
		if player.id == get_tree().get_network_unique_id():
			continue
		for card in (player.hand as Hand).cards:
			if state:
				card.connect("clicked", self, "_card_clicked", [player])
			else:
				card.disconnect("clicked", self, "_card_clicked")
			(card as Card).set_can_be_clicked(state)


func select_player():
	_set_selection_player_state(true)


func stop_select_player():
	_set_selection_player_state(false)


func _set_selection_player_state(selection_is_on: bool):
	for player in all_players.values():
		player = player as Player

		# Do not enable local player
		if player.id == get_tree().get_network_unique_id():
			player.set_can_be_selected(false)
			continue

		if selection_is_on:
			player.connect("player_selected", self, "_player_selected")
		else:
			player.disconnect("player_selected", self, "_player_selected")
		player.set_can_be_selected(selection_is_on)


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		Lobby.client_disconnect()
		get_tree().quit()
