class_name ActionWindow
extends Control

signal action_chosen


func _on_Revenu_pressed() -> void:
	emit_signal("action_chosen", ActionRevenu.new())


func _on_AideEtrangere_pressed() -> void:
	emit_signal("action_chosen", ActionAideEtrangere.new())


func _on_Assassinat_pressed() -> void:
	emit_signal("action_chosen", ActionAssassinat.new())


func _on_Duchesse_pressed() -> void:
	emit_signal("action_chosen", ActionDuchesse.new())


func _on_Assassin_pressed() -> void:
	emit_signal("action_chosen", Action.new())


func _on_Capitaine_pressed() -> void:
	emit_signal("action_chosen", Action.new())


func _on_Ambassadeur_pressed() -> void:
	emit_signal("action_chosen", Action.new())


func _on_Inquisiteur_pressed() -> void:
	emit_signal("action_chosen", Action.new())
