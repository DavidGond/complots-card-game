tool
class_name PlayerPlace
extends Line2D

var placed_players = []


func get_direction():
	return points[0].angle_to_point(points[1])


func add_player(player: Player):
	var direction = get_direction()
	player.set_rotation(direction)
	placed_players.append(player)
	_redraw()


func _redraw():
	var nb_players = placed_players.size()
	var center = (points[0] + points[1]) * 0.5
	var space_between_players = Hand.CARD_SIZE * 1.5
	var line = points[0].distance_to(points[1])
	var needed_space = space_between_players * nb_players
	var space = line - needed_space
	var nb_placed = 0
	for player in placed_players:
		player.set_position(
			(
				position
				+ lerp(
					points[0].move_toward(center, space * 0.5),
					points[1].move_toward(center, space * 0.5),
					nb_placed / (nb_players - 1) if nb_players > 1 else 0.5
				)
			)
		)
		nb_placed += 1
