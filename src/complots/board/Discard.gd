class_name Discard
extends CardZone

onready var node_cards = $Cards


func add_card(card: Card):
	if card:
		cards.push_back(card)
		node_cards.add_child(card)
		card.flip_to_front()


func remove_card(card: Card):
	var index = cards.find(card)
	if index >= 0:
		cards.remove(index)
		node_cards.remove_child(card)
