class_name PickCardWindow
extends Control

signal card_picked(card)

onready var grid = $ScrollContainer/Grid
onready var hand = $Hand/Hand as Hand


func _ready():
	hand.set_local_player(true)


func set_cards(cards: Array):
	for character in cards:
		var card = Const.get_card_instance()
		card.init(character)
		grid.add_child(card)
		card.connect("clicked", self, "_on_Card_clicked")
		card.flip_to_front()
		card.set_can_be_clicked(true)


func _on_Card_clicked(card: Card):
	emit_signal("card_picked", card)
	print(card)


func get_hand() -> Hand:
	return hand
