class_name Deck
extends CardZone

const MAX_DISPLAY := 10
const DISPLAY_STEP := Vector2(1, 1)

export var can_pick: bool = false

onready var node_cards := $Cards


func redraw():
	for child in node_cards.get_children():
		node_cards.remove_child(child)
	var to_display = min(MAX_DISPLAY, cards.size()) - 1
	var pos_diff := Vector2.ZERO
	for i in range(to_display, -1, -1):
		var card = cards[i] as Card
		card.set_position(node_cards.rect_position + pos_diff)
		node_cards.add_child(card)
		yield(card, "ready")
		card.disable_anim()
		pos_diff -= DISPLAY_STEP


func add_card_bottom(card: Card):
	if card:
		cards.push_back(card)
		redraw()


func add_card_top(card: Card):
	if card:
		cards.push_front(card)
		redraw()


func remove_card_top() -> Card:
	var card: Card = cards.pop_front()
	if card:
		card.enable_anim()
		redraw()
	return card


# Appends an array of cards (as characters)
puppetsync func append_cards(new_cards: Array):
	for character in new_cards:
		var card = Const.get_card_instance()
		card.init(character)
		cards.append(card)
	redraw()
	print(cards.size())


func shuffle():
	randomize()  # Reseed so that each shuffle is different
	cards.shuffle()


func _on_Deck_gui_input(event):
	if (
		event is InputEventMouseButton
		and event.button_index == BUTTON_LEFT
		and event.is_pressed()
		and can_pick
	):
		print("Clicked on Deck")
		var removed_card = remove_card_top()


func _on_Deck_mouse_entered():
	print("Deck hovered")
