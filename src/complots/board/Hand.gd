class_name Hand
extends CardZone

const CARD_SIZE = 50

var _is_player_local := false

onready var node_cards = $Cards


func _ready():
	pass


func _redraw():
	var nb_cards = cards.size() - 1
	var card_pos = node_cards.position - Vector2(CARD_SIZE * nb_cards * 0.5, 0)
	for card in cards:
		card = card as Card
		if _is_player_local:
			card.flip_to_front()
		else:
			card.flip_to_back()
		card.set_position(card_pos)
		card_pos.x += CARD_SIZE


func add_card(card: Card):
	if card:
		cards.push_back(card)
		node_cards.add_child(card)
		_redraw()


func remove_card(card: Card):
	var index = cards.find(card)
	if index >= 0:
		cards.remove(index)
		node_cards.remove_child(card)
	_redraw()


func set_local_player(is_player_local: bool):
	_is_player_local = is_player_local
