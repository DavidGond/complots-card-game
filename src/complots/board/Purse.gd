class_name Purse
extends Node2D

var coins: int = 0 setget set_coins
onready var coins_label: Label = $Coins

func _ready():
	update_ui()

# Adds a certain amount of coins and returns the total of stored coins.
func add_coins(amount: int) -> int:
	coins += amount
	update_ui()
	return coins

# Removes a certain amount of coins and returns the total of stored coins.
# The total cannot be lower than 0.
func remove_coins(amount: int) -> int:
	coins = int(max(0, coins - amount))
	update_ui()
	return coins

# Sets the new amount of stored coins and returns it. 
# The amount cannot be lower than 0.
func set_coins(value: int) -> int:
	coins = int(max(0, value))
	update_ui()
	return coins

func update_ui():
	if coins_label:
		coins_label.text = str(coins)
