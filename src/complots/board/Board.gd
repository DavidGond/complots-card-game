class_name Board
extends Control

var is_ready: bool = false
var game

onready var back_place := $Back
onready var left_place := $Left
onready var right_place := $Right
onready var front_place := $Front

onready var action_window := $UI/ActionWindow
onready var pick_card_window := $UI/PickCardWindow

onready var players_store = $Players

onready var deck := $Deck


func _ready():
	is_ready = true  # To make sure everything is set up before using the UI
	display_action_window(false)


# @var{other_players} does not contain the local player.
func init_board(new_game, local_player, other_players):
	if not is_ready:
		pass
	game = new_game
	place_players(local_player, other_players)


# Places the players depending on how many there are.
func place_players(local_player: Player, other_players):
	front_place.add_player(local_player)
	players_store.add_child(local_player)

	var nb_players = other_players.size()
	var places := {0: back_place, 1: left_place, 2: right_place}
	var position_players := {
		1: [1, 0, 0],
		2: [2, 0, 0],
		3: [1, 1, 1],
		4: [2, 1, 1],
		5: [3, 1, 1],
		6: [3, 2, 1],
		7: [3, 2, 2]
	}
	var positions = position_players[nb_players]

	var remaining_positions = positions
	for p in other_players:
		var player: Player = p
		for i in range(remaining_positions.size()):
			if remaining_positions[i] > 0:
				var place: PlayerPlace = places[i]
				place.add_player(player)
				players_store.add_child(player)
				remaining_positions[i] -= 1
				break  # Early quit, player is placed


func display_action_window(new_state: bool = true):
	action_window.visible = new_state


func display_pick_card_window(new_state: bool = true):
	pick_card_window.visible = new_state


# Adds the characters to the window
func pick_card_from_array(cards: Array):
	var pick_script = pick_card_window as PickCardWindow
	pick_script.set_cards(cards)

	var hand = pick_script.get_hand()
	for card in game.local_player.player.hand.cards:
		var character = card.card_character
		var hand_card = Const.get_card_instance()
		hand_card.init(character)
		hand.add_card(hand_card)

	pick_card_window.visible = true
