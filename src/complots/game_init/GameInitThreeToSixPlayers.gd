class_name GameInitThreeToSixPlayers
extends GameInit


func _begin_init(players):
	# Merge 3 decks into one
	var deck: Array = []
	for i in range(3):
		deck.append_array(game.create_simple_deck())
	randomize()
	deck.shuffle()

	# Give 2 cards to each player
	for i in range(2):
		for player in players.values():
			game.give_card_to_player(deck.pop_back(), player)

	# Place the remaining cards in the board
	var board_deck = game.board.deck as Deck
	board_deck.rpc("append_cards", deck)

	_complete()
