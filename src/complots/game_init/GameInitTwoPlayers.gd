class_name GameInitTwoPlayers
extends GameInit

var players_ready := {}
var all_players


func _begin_init(players):
	all_players = players
	game.connect("card_pick_confirmed", self, "_card_picked")

	# Randomly give a card to each player
	var deck = game.create_simple_deck()
	randomize()
	deck.shuffle()
	var random_cards = [deck.pop_front(), deck.pop_front()]
	var player_index = 0
	for player in players.values():
		game.give_card_to_player(deck.pop_back(), player)
		player_index += 1

	# Place the remaining cards in the board
	var board_deck = game.board.deck as Deck
	board_deck.rpc("append_cards", deck)

	# Tell players to pick a card from a complete deck
	for player_id in players:
		game.rpc_id(player_id, "pick_card", game.create_simple_deck())


func _card_picked(player_id, _character):
	players_ready[player_id] = player_id
	check_all_ready()


func check_all_ready():
	if players_ready.size() == all_players.size():
		_complete()
