class_name GameInit
extends Node2D

# This class is the parent of each way of initiating the game, depending on
# the number of players.

var game


# Overriden by children to start the init.
func _begin_init(_players):
	pass


func init(new_game, players):
	game = new_game
	_begin_init(players)


func _complete():
	game.next_turn()
