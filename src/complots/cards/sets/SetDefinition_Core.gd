extends Reference
class_name DefinitionsCore

const DOUBT_ALL = "Peut être mis en doute par n'importe quel joueur"
const DOUBT_TARGET = "Peut être mis en doute par la cible"

const COUNTER_NONE = "Aucun contre"
const COUNTER_DEFAULT = "Contré par"

enum CHARACTERS {
	DUCHESSE,
	INQUISITEUR,
	ASSASSIN,
	AMBASSADEUR,
	COMPTESSE,
	CAPITAINE,
}

const SET = "Basic Game"
const CARDS := {
	CHARACTERS.DUCHESSE: {
		"Name": "La Duchesse",
		"Type": "Character",
		"Abilities": "Obtient 3 pièces.",
		"Counters": COUNTER_NONE,
		"Doubts": DOUBT_ALL,
	},
	CHARACTERS.INQUISITEUR: {
		"Name": "L'Inquisiteur",
		"Type": "Character",
		"Abilities": "Utilisable d’1 seule des 2 façons : " \
				+ "\n\na) Pioche une carte et en défausse une." \
				+ "La pioche est mélangée." \
				+ "\n\nb) Regarde une carte personnage chez un adversaire qui " \
				+ "choisit quelle carte il désire montrer. " \
				+ "L'Inquisiteur choisit soit de la rendre, soit de la faire défausser " \
				+ "(dans ce cas la carte est rangée, une nouvelle carte est piochée et la pioche est mélangée)",
		"Counters": COUNTER_NONE,
		"Doubts": DOUBT_ALL + " (si pioche),\n" + DOUBT_TARGET + "sinon",
	},
	CHARACTERS.ASSASSIN: {
		"Name": "L'Assassin",
		"Type": "Character",
		"Abilities": "Paye 3 pièces et assassine un personnage adverse.",
		"Counters": COUNTER_DEFAULT + " la Comptesse",
		"Doubts": DOUBT_TARGET
	},
	CHARACTERS.AMBASSADEUR: {
		"Name": "L'Ambassadeur",
		"Type": "Character",
		"Abilities": "Pioche 2 cartes et en remet 2. La pioche est ensuite mélangée.",
		"Counters": COUNTER_NONE,
		"Doubts": DOUBT_ALL
	},
	CHARACTERS.COMPTESSE: {
		"Name": "La Comptesse",
		"Type": "Character",
		"Abilities": "Contre l’Assassin. L’assassinat échoue, mais les pièces sont quand même payées.",
		"Counters": COUNTER_NONE,
		"Doubts": DOUBT_TARGET
	},
	CHARACTERS.CAPITAINE: {
		"Name": "Le Capitaine",
		"Type": "Character",
		"Abilities": "Prend 2 pièces à un adversaire.",
		"Counters": COUNTER_DEFAULT + " le Capitaine, l'Ambassadeur, l'Inquisiteur",
		"Doubts": DOUBT_TARGET
	},
}
