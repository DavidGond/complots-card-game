class_name Card
extends Control

signal clicked(card)

enum FACES { FRONT = -1, BACK = 1 }

export var highlight_default_color: Color
export var highlight_hover_color: Color

var current_face = FACES.BACK

# The character indicates which front the card will have and which scripts
# are related to it.
var card_character: int = 0
var _can_be_clicked: bool = false

onready var front = $Frame/Body/CardFront
onready var front_sprite: TextureRect = $Frame/Body/CardFront/Front
onready var back = $Frame/Body/CardBack
onready var anim_player = $Frame/Body/AnimationPlayer
onready var highlight = $Frame/Body/Highlight


func _ready():
	_update_front()
	set_can_be_clicked(false)


func init(character):
	if character in Const.CARD_TEXTURES:
		card_character = character
		_update_front()


func flip_to_front():
	front.set_visible(true)
	back.set_visible(false)
	current_face = FACES.FRONT


func flip_to_back():
	front.set_visible(false)
	back.set_visible(true)
	current_face = FACES.BACK


func flip(target_side = 0):
	if target_side == 0:
		flip(-current_face)
		return
	if target_side == FACES.FRONT:
		flip_to_front()
	else:
		flip_to_back()


func set_can_be_clicked(can_click: bool):
	if can_click:
		_enable_highlight()
	else:
		_disable_highlight()


func _update_front():
	if front_sprite:
		front_sprite.texture = Const.CARD_TEXTURES[card_character]


func _on_Area_mouse_entered():
	anim_player.play("Zoom_in")
	if _can_be_clicked:
		highlight.color = highlight_hover_color


func _on_Area_mouse_exited():
	anim_player.play_backwards("Zoom_in")
	highlight.color = highlight_default_color


func _on_Area_gui_input(event):
	if (
		event is InputEventMouseButton
		and event.button_index == BUTTON_LEFT
		and event.is_pressed()
		and _can_be_clicked
	):
		emit_signal("clicked", self)


func _enable_highlight():
	_can_be_clicked = true
	highlight.visible = true


func _disable_highlight():
	_can_be_clicked = false
	highlight.visible = false
