class_name Player
extends Control

signal coins_given(amount)
signal player_selected(player)

var id: int
var nickname: String setget set_nickname

onready var hand = $Hand as Hand
onready var purse = $Purse
onready var nickname_ui = $Nick_no_rotation/Nickname
onready var nick_no_rotation = $Nick_no_rotation
onready var discard_zone = $Discard as Discard


func _ready():
	# Reset some ui rotation so that their content can be read
	purse.global_rotation = 0
	(nick_no_rotation as Container).set_rotation(2 * PI - self.get_rotation())

	set_nickname(nickname)
	if id == get_tree().get_network_unique_id():
		hand.set_local_player(true)


func init(new_id: int, new_nickname: String):
	id = new_id
	set_nickname(new_nickname)


puppetsync func give_coins(amount: int):
	purse.add_coins(amount)
	emit_signal("coins_given", amount)

puppetsync func give_card(character):
	print("Given card: ", Const.DEFINITIONS.CARDS[character]["Name"])
	var card = Const.get_card_instance()
	card.init(character)
	hand.add_card(card)

puppetsync func give_hidden_card():
	var self_id = get_tree().get_network_unique_id()
	# Ignore the call if the player is the local one or server
	if self_id == id or self_id == 1:
		return

	print("Player " + nickname + " received a card")
	# Add the card to their hand
	var card = Const.get_card_instance()
	hand.add_card(card)

puppetsync func discard_card(index: int, character):
	if index < 0 or index >= hand.cards.size():
		print("Card discard index not valid: " + str(index))
	var card = hand.cards[index]
	print(
		"Player " + name + " discarded card: ", Const.DEFINITIONS.CARDS[card.card_character]["Name"]
	)
	hand.remove_card(card)

	# Instanciate a new card: other players did not know which character it was.
	var discarded = Const.get_card_instance()
	discarded.init(character)
	discard_zone.add_card(discarded)


func set_nickname(nick: String = "NoName"):
	nickname = nick
	if nickname_ui:
		nickname_ui.text = nickname


func set_can_be_selected(can_be_selected: bool):
	nickname_ui.disabled = !can_be_selected


func _on_Nickname_pressed():
	emit_signal("player_selected", self)
