extends Node

# This singleton is used to contain values that should always be kept available

enum Options { AMBASSADEUR_INQUISITEUR }

const DEFINITIONS = DefinitionsCore

const CARD_TEXTURES := {
	DEFINITIONS.CHARACTERS.AMBASSADEUR: preload("res://assets/card_fronts/ambassadeur.png"),
	DEFINITIONS.CHARACTERS.ASSASSIN: preload("res://assets/card_fronts/assassin.png"),
	DEFINITIONS.CHARACTERS.CAPITAINE: preload("res://assets/card_fronts/capitaine.png"),
	DEFINITIONS.CHARACTERS.COMPTESSE: preload("res://assets/card_fronts/comptesse.png"),
	DEFINITIONS.CHARACTERS.DUCHESSE: preload("res://assets/card_fronts/duchesse.png"),
	DEFINITIONS.CHARACTERS.INQUISITEUR: preload("res://assets/card_fronts/inquisiteur.png"),
}

const ACCEPTABLE_VALUES := {
	Options.AMBASSADEUR_INQUISITEUR:
	{
		DEFINITIONS.CHARACTERS.AMBASSADEUR: "Ambassadeur",
		DEFINITIONS.CHARACTERS.INQUISITEUR: "Inquisiteur",
	},
}


func get_card_instance() -> Card:
	return preload("res://src/complots/cards/Card.tscn").instance() as Card


func get_player_instance() -> Player:
	return preload("res://src/complots/Player.tscn").instance() as Player


func get_game():
	return preload("res://src/complots/Game.tscn")


func get_client_lobby():
	return preload("res://src/complots/network/ClientLobby.tscn")


func get_server_lobby():
	return preload("res://src/complots/network/ServerLobby.tscn")


func get_action_window():
	return preload("res://src/complots/board/ActionWindow.tscn")


func get_main():
	return preload("res://src/Main.tscn")
