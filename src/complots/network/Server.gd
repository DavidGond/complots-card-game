class_name Server
extends Node

const SERVER_PORT: = 2323
const MAX_PLAYERS: = 8

func init(port: int = SERVER_PORT, max_players: int = MAX_PLAYERS) -> void:
	print("Starting the server...")
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, max_players)
	get_tree().network_peer = peer
	Lobby.register_player(Lobby.my_info)
	print("Server started")

func stop() -> void:
	print("Stopping the server...")
	get_tree().network_peer.close_connection()
	print("Server stopped")
