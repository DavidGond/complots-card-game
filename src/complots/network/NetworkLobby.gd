extends Node

signal player_list_changed
signal connected

# Player info, associate ID to data
var player_info = {}

var maximum_players = 2

var game_options := {
	Const.Options.AMBASSADEUR_INQUISITEUR: Const.DEFINITIONS.CHARACTERS.AMBASSADEUR,
}

# Info we send to other players
var my_info = {name = "ComplotsPlayer"}


# Connect all functions
func _ready():
	handle_error(get_tree().connect("network_peer_connected", self, "_player_connected"))
	handle_error(get_tree().connect("network_peer_disconnected", self, "_player_disconnected"))
	handle_error(get_tree().connect("connected_to_server", self, "_connected_ok"))
	handle_error(get_tree().connect("connection_failed", self, "_connected_fail"))
	handle_error(get_tree().connect("server_disconnected", self, "_server_disconnected"))


func _player_connected(id):
	# Called on both clients and server when a peer connects. Send my info to it.
	print("player connected: " + String(id))
	if get_tree().get_network_unique_id() == 1:
		if player_info.size() >= maximum_players:
			get_tree().set_refuse_new_network_connections(true)
			print("Lobby is full, disconnecting player...")
			rpc_id(id, "kicked", "Lobby is full")
			get_tree().get_network_peer().disconnect_peer(id)
			return
		get_tree().set_refuse_new_network_connections(false)
	else:
		rpc_id(1, "register_player", my_info)


func _player_disconnected(id):
	print("player disconnected: " + String(id))
	player_info.erase(id)  # Erase player from info.
	emit_signal("player_list_changed")


func _connected_ok():
	# Only called on clients, not server.
	print("connected to server")


func _server_disconnected():
	# Server kicked us; abort.
	print("disconnected from server")


remote func kicked(reason: String):
	print("Kicked from server: " + reason)


func _connected_fail():
	print("connection failed")


master func register_player(info):
	print("registering player: ", info.name)
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	if id == 0:
		id = get_tree().get_network_unique_id()
	# Store the info
	player_info[id] = info
	emit_signal("player_list_changed")

	rpc("update_player_list", player_info)

	# Send game options to new player
	if id != 1:
		rpc_id(id, "update_game_options", game_options)
		rpc_id(id, "_connection_complete")


remote func update_player_list(players):
	print("connected players: ", players)
	# Store the info
	player_info = players
	emit_signal("player_list_changed")


puppet func update_game_options(options):
	game_options = options


func change_game_option(option, value):
	rpc("_on_game_option_changed", option, value)


puppet func _connection_complete():
	emit_signal("connected")


puppetsync func _on_game_option_changed(option, value):
	var previous_value = game_options[option]
	print(
		(
			"Changed option: %s from %d(%s) to %d(%s)"
			% [
				Const.Options.keys()[option],
				previous_value,
				Const.ACCEPTABLE_VALUES[option][previous_value],
				value,
				Const.ACCEPTABLE_VALUES[option][value],
			]
		)
	)
	game_options[option] = value


func handle_error(error):
	if error:
		print(error)
		print_stack()


# Closes the connection
func client_disconnect() -> void:
	var peer = get_tree().network_peer
	if peer == null:
		print("Already disconnected")

	peer.close_connection()
	get_tree().network_peer = null
	print("Disconnected from server")
