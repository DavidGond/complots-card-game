class_name Client
extends Node

const SERVER_IP: = "127.0.0.1"
const SERVER_PORT: = 2323

func init(ip: String = SERVER_IP, port: int = SERVER_PORT) -> void:
	print("Connecting to " + ip + String(port) + "...")
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, port)
	get_tree().network_peer = peer
