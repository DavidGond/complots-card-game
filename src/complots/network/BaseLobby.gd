class_name BaseLobby
extends Control

var host_color = Color.aqua
var self_color = Color.chartreuse

onready var option_ambassadeur_inquisiteur: OptionButton = $GameOptions/AmbassadeurInquisiteur/Value
onready var _player_list = $Players/PlayersList
onready var _connection_filter = $ConnectionFilter


func _ready():
	get_tree().set_auto_accept_quit(false)
	option_ambassadeur_inquisiteur.rset_config("selected", MultiplayerAPI.RPC_MODE_PUPPET)

	# Fill the option_ambassadeur_inquisiteur items
	var option = Const.Options.AMBASSADEUR_INQUISITEUR
	var option_values = Const.ACCEPTABLE_VALUES[option]
	for id in option_values:
		option_ambassadeur_inquisiteur.add_item(option_values[id], id)

	Lobby.connect("player_list_changed", self, "_player_list_changed")
	Lobby.connect("connected", self, "_on_connection")

	_player_list_changed()


func _exit_tree():
	Lobby.disconnect("player_list_changed", self, "_player_list_changed")


func _player_list_changed():
	if _player_list:
		for child in _player_list.get_children():
			_player_list.remove_child(child)
		for player in Lobby.player_info:
			var label = Label.new()
			label.text = Lobby.player_info[player]["name"]
			if player == 1:  # Host
				label.add_color_override("font_color", host_color)
			elif player == get_tree().network_peer.get_unique_id():
				label.add_color_override("font_color", self_color)
			_player_list.add_child(label)


func _on_connection():
	# Update the selected option value
	option_ambassadeur_inquisiteur.select(
		option_ambassadeur_inquisiteur.get_item_index(
			Lobby.game_options[Const.Options.AMBASSADEUR_INQUISITEUR]
		)
	)

	_player_list_changed()
	_connection_filter.hide()


func _on_Start_pressed():
	if get_tree().get_network_unique_id() == 1:
		rpc("start_game", Lobby.player_info)


puppetsync func start_game(players):
	Lobby.player_info = players
	get_tree().change_scene_to(Const.get_game())


func _on_Exit_pressed():
	Lobby.client_disconnect()
	get_tree().change_scene_to(Const.get_main())


func _on_option_ambassadeur_inquisiteur_value_changed(index):
	_update_option_ambassadeur_inquisiteur(option_ambassadeur_inquisiteur.get_item_id(index))


func _update_option_ambassadeur_inquisiteur(_value):
	# Available only on host
	pass


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		Lobby.client_disconnect()
		get_tree().quit()
