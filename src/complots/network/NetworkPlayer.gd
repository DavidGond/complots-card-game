class_name NetworkPlayer
extends Node2D

signal played  # Signal emitted when completing the turn

var player: Player
var game: Game


func _ready():
	pass


func play():
	print("It's my turn! | ", get_tree().get_network_unique_id(), " | ", player.id)
	game.board.action_window.connect("action_chosen", self, "_on_action_chosen")
	game.board.display_action_window(true)


func _on_action_chosen(action: Action):
	# Disconnect from the action window
	game.board.action_window.disconnect("action_chosen", self, "_on_action_chosen")
	game.board.display_action_window(false)

	# Call the action
	action.connect("action_completed", self, "_on_action_completed")
	action.execute(game, player)


func _on_action_completed():
	emit_signal("played")
