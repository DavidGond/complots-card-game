class_name ServerLobby
extends BaseLobby

onready var value_ip = $IP/ValueIP


func _ready():
	if get_tree().get_network_unique_id() == 1:
		# Fill IP addresses
		var ip_regexp = RegEx.new()
		ip_regexp.compile("^(?:[0-9]{1,3}.){3}[0-9]{1,3}$")
		var ips: PoolStringArray = IP.get_local_addresses()
		var str_ip = ""
		for ip in ips:
			var regex_match = ip_regexp.search(ip)
			if regex_match:
				str_ip += regex_match.subject + "\n"
		value_ip.text = str_ip

		# Hide filter
		_connection_filter.hide()


func _update_option_ambassadeur_inquisiteur(value):
	Lobby.change_game_option(Const.Options.AMBASSADEUR_INQUISITEUR, value)

	# Update the selected item on the clients
	var index = option_ambassadeur_inquisiteur.get_item_index(value)
	option_ambassadeur_inquisiteur.rset("selected", index)
